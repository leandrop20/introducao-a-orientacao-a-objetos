package aula14;

public class RMV implements IPlayerVideo {
	
	@Override
	public void play() {
		System.out.println("play...");
	}

	@Override
	public void stop() {
		System.out.println("stop!.");
	}

	@Override
	public void pause() {
		System.out.println("pause..");
	}

	@Override
	public void volume(int volume) {
		System.out.println("Volume: "+volume);
	}

	@Override
	public void taxaDeBits() {
		System.out.println("128 bits");
	}
	
	@Override
	public void taxaDeQuadros() {
		System.out.println("24 q/s");
	}

	@Override
	public void nomeArquivo(String arquivo) {
		System.out.println("Arquivo: "+arquivo);
	}

}
