package aula14;

public class MP3 implements IPlayer, IAudio {

	@Override
	public void play() {
		System.out.println("play...");
	}

	@Override
	public void stop() {
		System.out.println("stop!.");
	}

	@Override
	public void pause() {
		System.out.println("pause..");
	}

	@Override
	public void volume(int volume) {
		System.out.println("Volume: "+volume);
	}

	@Override
	public void taxaDeBits() {
		System.out.println("128 bits");
	}
	
}
