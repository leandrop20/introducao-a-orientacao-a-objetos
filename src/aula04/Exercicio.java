package aula04;

public class Exercicio {
	
	public Exercicio() {
		
	}
	
	private void tabuada(int a) {
		for (int i = 1; i < 11; i++) {
			System.out.println(i + "*" + a + "=" + i*a);
		}
	}
	
	public static void main(String[] args) {
		Exercicio ex = new Exercicio();
		ex.tabuada(2);
	}
	
}
