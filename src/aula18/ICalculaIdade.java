package aula18;

public interface ICalculaIdade {

	int getAnos();
	int getMeses();
	int getDias();
	
}
