package aula14;

public interface IPlayer {
	
	public abstract void play();
	abstract void stop();
	void pause();
	void volume(int volume);

}
