package aula16;

public class Aula {
	
	public static String nome = "Ana";
	public String sobrenome = "Monteiro";
	
	void imprime() {
		System.out.println(nome + " " + sobrenome);
	}
	
	public static void main(String[] args) {
		
		//System.out.println(nome + " " + sobrenome);
		
		//new Aula().imprime();
		//System.out.println(nome + " " +new Aula().sobrenome);
		
		//show();
		
		MyClass myClass = new MyClass();
		myClass.imprime();
	}

	static void show() {
		System.out.println(nome + " "+new Aula().sobrenome);
	}
	
}
