package aula19;

public class Aula {

	public static void main(String[] args) {
		
		System.out.println("Soma = " + new Aula().getCalculo(9, 5));
		
	}
	
	int getCalculo(int x, int y) {
		
		class Calculo {
			int soma() {
				return x+y;
			}
		}
		
		return new Calculo().soma();
		
	}
	
}
