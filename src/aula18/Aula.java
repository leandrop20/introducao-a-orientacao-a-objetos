package aula18;

import java.time.LocalDate;

public class Aula {

	public static void main(String[] args) {
		Pessoa p = new Pessoa();
		p.setNome("Ana");
		p.setSobrenome("Pinheiro");
		p.setNascimento(LocalDate.of(1990, 5, 15));
		
		System.out.printf("%s %s possui %d anos, %d meses, %d dias.",
				p.getNome(),
				p.getSobrenome(),
				p.getIdade().getAnos(),
				p.getIdade().getMeses(),
				p.getIdade().getDias());
	}
	
}
