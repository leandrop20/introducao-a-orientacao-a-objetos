package aula08;

public class Aula {
	
	public static void main(String[] args) {
		
		Livro l1 = new Livro();
		l1.setTitulo("Java 1");
		l1.setAutor("Beltrano");
		l1.setPaginas(50);
		l1.setLancamento(false);
		
		System.out.println(l1.toString());
		
		Livro l2 = new Livro("Java 2");
		System.out.println(l2.toString());
		
		Livro l3 = new Livro(30);
		System.out.println(l3.toString());
		
		Livro l4 = new Livro("Java 3", "Fulano", 10, true);
		System.out.println(l4);
	}

}
